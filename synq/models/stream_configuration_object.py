# coding: utf-8

"""
Copyright 2016 SmartBear Software

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

    Ref: https://github.com/swagger-api/swagger-codegen
"""

from pprint import pformat
from six import iteritems


class StreamConfigurationObject(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """
    def __init__(self):
        """
        StreamConfigurationObject - a model defined in Swagger

        :param dict swaggerTypes: The key is attribute name
                                  and the value is attribute type.
        :param dict attributeMap: The key is attribute name
                                  and the value is json key in definition.
        """
        self.swagger_types = {
            'rtmp_host': 'str',
            'rtmp_port': 'str',
            'rtmp_key': 'str',
            'rtmp_url': 'str'
        }

        self.attribute_map = {
            'rtmp_host': 'rtmp_host',
            'rtmp_port': 'rtmp_port',
            'rtmp_key': 'rtmp_key',
            'rtmp_url': 'rtmp_url'
        }

        self._rtmp_host = None
        self._rtmp_port = None
        self._rtmp_key = None
        self._rtmp_url = None

    @property
    def rtmp_host(self):
        """
        Gets the rtmp_host of this StreamConfigurationObject.


        :return: The rtmp_host of this StreamConfigurationObject.
        :rtype: str
        """
        return self._rtmp_host

    @rtmp_host.setter
    def rtmp_host(self, rtmp_host):
        """
        Sets the rtmp_host of this StreamConfigurationObject.


        :param rtmp_host: The rtmp_host of this StreamConfigurationObject.
        :type: str
        """
        self._rtmp_host = rtmp_host

    @property
    def rtmp_port(self):
        """
        Gets the rtmp_port of this StreamConfigurationObject.


        :return: The rtmp_port of this StreamConfigurationObject.
        :rtype: str
        """
        return self._rtmp_port

    @rtmp_port.setter
    def rtmp_port(self, rtmp_port):
        """
        Sets the rtmp_port of this StreamConfigurationObject.


        :param rtmp_port: The rtmp_port of this StreamConfigurationObject.
        :type: str
        """
        self._rtmp_port = rtmp_port

    @property
    def rtmp_key(self):
        """
        Gets the rtmp_key of this StreamConfigurationObject.


        :return: The rtmp_key of this StreamConfigurationObject.
        :rtype: str
        """
        return self._rtmp_key

    @rtmp_key.setter
    def rtmp_key(self, rtmp_key):
        """
        Sets the rtmp_key of this StreamConfigurationObject.


        :param rtmp_key: The rtmp_key of this StreamConfigurationObject.
        :type: str
        """
        self._rtmp_key = rtmp_key

    @property
    def rtmp_url(self):
        """
        Gets the rtmp_url of this StreamConfigurationObject.


        :return: The rtmp_url of this StreamConfigurationObject.
        :rtype: str
        """
        return self._rtmp_url

    @rtmp_url.setter
    def rtmp_url(self, rtmp_url):
        """
        Sets the rtmp_url of this StreamConfigurationObject.


        :param rtmp_url: The rtmp_url of this StreamConfigurationObject.
        :type: str
        """
        self._rtmp_url = rtmp_url

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other

