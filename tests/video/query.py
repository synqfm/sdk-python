import unittest
import os

try:
    synq_api_key = os.environ['SYNQ_API_KEY']
except KeyError as e:
    print("** ERROR: Environment variable $SYNQ_API_KEY must be set to run tests!")
    exit(1)

class TestQueryMethod(unittest.TestCase):

    def test_example(self):
        # START EXAMPLE FOR videos__query_exact
        import synq, json
        va = synq.VideoApi()
        # Retrieve all videos
        res = va.query(synq_api_key, json.dumps({}))
        # Retrieve the _title field and number of views from all videos that have file-state "uploaded"
        res = va.query(synq_api_key, """
if(video.file_state == "uploaded")
return video.pick("views", "embed_url")
""")
        print(res)
        # STOP EXAMPLE FOR videos__query_exact
        assert(type(res) == type([]))

    # def test_query_exact_for_nonexistant(self):
    #     import synq, json, uuid
    #     va = synq.VideoApi()
    #     res = va.query_exact(synq_api_key, json.dumps({"completely bogus": uuid.uuid4().hex}))
    #     # shouldn't match anything!
    #     assert(res == [])

    # def test_query_exact_for_userdata(self):
    #     import synq, json, uuid
    #     va = synq.VideoApi()
    #     my_uuid = uuid.uuid4().hex
    #     video_id = va.create(synq_api_key, metadata=json.dumps({"userdata": {"this is a test": my_uuid}}))['video_id']
    #     res = va.query_exact(synq_api_key, json.dumps({"userdata": {"this is a test": my_uuid}}))
    #     assert(len(res) == 1)
    #     assert(res[0]['userdata']['this is a test'] == my_uuid)

    # def test_query_exact_for_metadata(self):
    #     import synq, json, uuid
    #     va = synq.VideoApi()
    #     video_id = va.create(synq_api_key)['video_id']
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id}))
    #     assert(len(res) == 1)
    #     assert(res[0]['video_id'] == video_id)

    # def test_query_exact_for_extra_data(self):
    #     import synq, json, uuid
    #     va = synq.VideoApi()
    #     video_id_without_metadata = va.create(synq_api_key)['video_id']
    #     video_id_with_metadata = va.create(synq_api_key, metadata=json.dumps({"userdata": {"foo": "bar"}}))['video_id']
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id_without_metadata}), keys=json.dumps(["userdata"]))[0]
    #     assert(res == {'userdata': {}})
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id_with_metadata}), keys=json.dumps(["userdata"]))[0]
    #     assert(res == {'userdata': {'foo': 'bar'}})
    #     # test return-value filtering works for metadata
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id_without_metadata}), keys=json.dumps(["video_id"]))[0]
    #     assert(res == {'video_id': video_id_without_metadata})
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id_with_metadata}), keys=json.dumps(["video_id"]))[0]
    #     assert(res == {'video_id': video_id_with_metadata})
    #     # test mixing return-value filtering on userdata and metadata works
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id_without_metadata}), keys=json.dumps(["video_id", "userdata"]))[0]
    #     assert(res == {'video_id': video_id_without_metadata, 'userdata': {}})
    #     res = va.query_exact(synq_api_key, json.dumps({"video_id": video_id_with_metadata}), keys=json.dumps(["video_id", "userdata"]))[0]
    #     assert(res == {'video_id': video_id_with_metadata, 'userdata': {'foo': 'bar'}})

    

if __name__ == '__main__':
    unittest.main()
