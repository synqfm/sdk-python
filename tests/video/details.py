import unittest
import os

try:
    synq_api_key = os.environ['SYNQ_API_KEY']
except KeyError as e:
    print("** ERROR: Environment variable $SYNQ_API_KEY must be set to run tests!")
    exit(1)

class TestDetailsMethod(unittest.TestCase):

    def test_example(self):
        # START EXAMPLE FOR videos__details
        import synq, json
        va = synq.VideoApi()
        # First create a video we can query
        video_id = va.create(synq_api_key)['video_id']
        # Retrieve details about the video
        res = va.details(synq_api_key, video_id)
        # STOP EXAMPLE FOR videos__details
        assert(type(res) == type({}))

    def test_regression_empty_userdata_when_using_keys(self):
        # regression test for a bug where asking for keys=['video_id'] would return {"video_id":"...", "userdata":{"video_id":null}}.
        import synq, json
        va = synq.VideoApi()
        video_id = va.create(synq_api_key)['video_id']
        res = va.details(synq_api_key, video_id, keys="[\"video_id\"]")
        assert(res == {'video_id': video_id})

if __name__ == '__main__':
    unittest.main()
