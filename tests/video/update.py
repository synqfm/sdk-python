import unittest
import os

try:
    synq_api_key = os.environ['SYNQ_API_KEY']
except KeyError as e:
    print("** ERROR: Environment variable $SYNQ_API_KEY must be set to run tests!")
    exit(1)

class TestUpdateMethod(unittest.TestCase):

    def test_example(self):
        # START EXAMPLE FOR videos__update
        import synq, json
        va = synq.VideoApi()
        # Create a new video
        video_id = va.create(synq_api_key)['video_id']
        # Associate a new piece of metadata with the video
        va.update(synq_api_key, video_id, metadata=json.dumps({"userdata": {"next_in_playlist":"78d1ca98042648b7808ddff027a1c23f"}}))
        # STOP EXAMPLE FOR videos__update
        res = va.details(synq_api_key, video_id)
        assert(res['userdata']['next_in_playlist'] == "78d1ca98042648b7808ddff027a1c23f")

    def test_reject_incorrect_prefix(self):
        import synq, json
        va = synq.VideoApi()
        video_id = va.create(synq_api_key)['video_id']
        va.update(synq_api_key, video_id, metadata=json.dumps({"next_in_playlist":"78d1ca98042648b7808ddff027a1c23f"}))
        # this should really throw an exception!
        res = va.details(synq_api_key, video_id)
        assert('next_in_playlist' not in res['userdata'])

    def test_update_set_bogus_user_object(self):
        #TODO: what happens when user tries to set userdata to string in update or create?
        pass
if __name__ == '__main__':
    unittest.main()
